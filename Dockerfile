FROM ubuntu:18.04
ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

RUN apt update
RUN apt install -y zlib1g-dev upx-ucl software-properties-common build-essential
RUN add-apt-repository -y ppa:deadsnakes/ppa
RUN apt install -y python3.7-full python3.7-dev
RUN python3.7 -m ensurepip
RUN pip3 install poetry pyinstaller
RUN poetry config virtualenvs.create false

WORKDIR /code
ADD pyproject.toml /code/
ADD poetry.lock /code/

RUN poetry install --no-root --no-dev

ENTRYPOINT ["pyinstaller", "pyinstaller.spec"]
