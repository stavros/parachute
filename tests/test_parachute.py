import io
import re
from hashlib import md5

import json5
from click.testing import CliRunner

from parachute.cli import load_backup_from_file
from parachute.cli import mp
from parachute.cli import qgc
from parachute.cli import save_backup_to_file
from parachute.data_types import Backup
from parachute.data_types import Parameter


def test_backup_saving_loading():
    backup = Backup()
    backup.parameters = {
        "foo": Parameter(name="foo", value=1, index=1, type=1),
        "bar": Parameter(name="bar", value=2, index=1, type=1),
    }
    with io.StringIO() as outfile:
        save_backup_to_file(backup, outfile)
        outfile.seek(0)
        assert json5.load(outfile) == backup.as_dict()

        outfile.seek(0)
        assert backup == load_backup_from_file(outfile)


def test_filtering():
    backup = Backup()
    backup.parameters = {
        "foo": Parameter(name="foo", value=1, index=1, type=1),
        "bar": Parameter(name="bar", value=2, index=1, type=1),
    }

    backup.filter(re.compile("oo"))
    assert list(backup.parameters.keys()) == ["foo"]


def test_qgc(tmp_path):
    result = CliRunner().invoke(
        qgc, ["tests/files/backup.chute", str(tmp_path / "out")], obj={"FILTER": None}
    )
    assert result.exit_code == 0
    assert (
        md5((tmp_path / "out").read_bytes()).hexdigest()
        == md5(open("tests/files/qgc.params", "rb").read()).hexdigest()
    )


def test_mp(tmp_path):
    result = CliRunner().invoke(
        mp, ["tests/files/backup.chute", str(tmp_path / "out")], obj={"FILTER": None}
    )
    assert result.exit_code == 0
    assert (
        md5((tmp_path / "out").read_bytes()).hexdigest()
        == md5(open("tests/files/mp.param", "rb").read()).hexdigest()
    )
