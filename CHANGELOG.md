# Changelog


## Unreleased

### Fixes

* Complain if `get` and `set` are mixed up. [Stavros Korokithakis]


## v0.4.2 (2023-01-07)

### Features

* Add the `force-accept-calibration` command. [Stavros Korokithakis]

### Fixes

* Fix parameter display order. [Stavros Korokithakis]


## v0.4.1 (2022-11-05)

### Features

* Don't exit on missing parameters when restoring. [Stavros Korokithakis]

### Fixes

* Fix bug where negative numbers were erroneously not accepted. [Stavros Korokithakis]


## v0.4.0 (2022-07-02)

### Features

* Add getting and setting bits directly. [Stavros Korokithakis]

* Add the "--binary" parameter to display bit indexes. [Stavros Korokithakis]

### Fixes

* Remove unused code. [Stavros Korokithakis]


## v0.3.11 (2022-02-26)

### Features

* Add "--compare" flag to "restore" [Stavros Korokithakis]

### Fixes

* Name files a bit better. [Stavros Korokithakis]


## v0.3.10 (2021-12-17)

### Fixes

* Improve autodetection even more again. [Stavros Korokithakis]

* Improve autodetection even more. [Stavros Korokithakis]

* Improve autodetection default. [Stavros Korokithakis]


## v0.3.9 (2021-11-02)

### Fixes

* Show the correct parameter name when diffing. [Stavros Korokithakis]


## v0.3.8 (2021-10-29)

### Features

* Colorize tables. [Stavros Korokithakis]

* Make table Markdown-compatible. [Stavros Korokithakis]

### Fixes

* Fix inverted `compare` display. [Stavros Korokithakis]


## v0.3.7 (2021-10-23)

### Features

* Include parameter completions. [Stavros Korokithakis]

* Add `--baud-rate` cli option` [Stavros Korokithakis]

### Fixes

* Display accurate names when diffing. [Stavros Korokithakis]

* Fix port detection on Windows. [Stavros Korokithakis]


## v0.3.6 (2021-08-29)

### Features

* Attempt socket autodetection. [Stavros Korokithakis]

### Fixes

* Make messages more consistent. [Stavros Korokithakis]


## v0.3.5 (2021-05-24)

### Features

* Show old and new parameter values when setting. [Stavros Korokithakis]


## v0.3.4 (2021-05-04)

### Features

* Add --only-backup and --only-craft options to "compare" [Stavros Korokithakis]

* Add version command line parameter. [Stavros Korokithakis]

* Allow the "compare" command to compare two backups. [Stavros Korokithakis]


## v0.3.2 (2021-03-15)

### Fixes

* Make MP files actually compatible with MP. [Stavros Korokithakis]

* Add timeout on parameter fetching. [Stavros Korokithakis]


## v0.3.1 (2021-03-15)

### Fixes

* Add more forbidden parameters. [Stavros Korokithakis]


## v0.3.0 (2021-03-14)

### Features

* Add "reset-to-defaults" option. [Stavros Korokithakis]

* Add "reboot" option. [Stavros Korokithakis]

### Fixes

* Fix table display. [Stavros Korokithakis]

* Print prettier tables. [Stavros Korokithakis]


## v0.2.2 (2021-03-06)

### Features

* Add the "show" command. [Stavros Korokithakis]

### Fixes

* Fix leftover characters in counts. [Stavros Korokithakis]

* Improve the display of the "get" command. [Stavros Korokithakis]


## v0.2.1 (2021-03-04)

### Features

* Add the --filter option to more commands. [Stavros Korokithakis]

### Fixes

* Don't print duplicate parameters when comparing. [Stavros Korokithakis]


## v0.2.0 (2021-03-04)

### Features

* Add a "compare" command. [Stavros Korokithakis]

* Add a "restore" command. [Stavros Korokithakis]

* Add a "set" command. [Stavros Korokithakis]

* Add a "get" command. [Stavros Korokithakis]


## v0.1.1 (2021-03-02)

### Features

* Add "filter" command. [Stavros Korokithakis]

* Allow converting to MP/QGC-compatible formats. [Stavros Korokithakis]

* Add "--debug" flag. [Stavros Korokithakis]

### Fixes

* Don't read too many status messages. [Stavros Korokithakis]

* Output the parameters correctly. [Stavros Korokithakis]

* Rename the output file. [Stavros Korokithakis]


## v0.1.0 (2021-03-01)

### Features

* Output JSON5 instead of JSON. [Stavros Korokithakis]


